package tech.mastertech.pontooauth.services;

import java.util.Collections;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import tech.mastertech.pontooauth.models.PontoOauth;
import tech.mastertech.pontooauth.repositories.PontoOauthRepository;

@Service
public class PontoOauthService implements UserDetailsService {

	@Autowired
	PontoOauthRepository pontoOauthRepository;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	public PontoOauth cadastroLogin(String funcional, String senha) {

		PontoOauth login = new PontoOauth();

		login.setFuncional(funcional);
		login.setSenha(passwordEncoder.encode(senha));

		return pontoOauthRepository.save(login);
	}

	@Override
	public UserDetails loadUserByUsername(String funcional) throws UsernameNotFoundException {
		// logger.info("buscanco funcional do usuário -->" + funcional);

		Optional<PontoOauth> optional = pontoOauthRepository.findByfuncional(funcional);

		if (!optional.isPresent()) {
			throw new UsernameNotFoundException("Usuário não encontrado");
		}

		PontoOauth ponto = optional.get();

		SimpleGrantedAuthority authority = new SimpleGrantedAuthority("user");

		return new User(ponto.getFuncional(), ponto.getSenha(), Collections.singletonList(authority));
	}

}
