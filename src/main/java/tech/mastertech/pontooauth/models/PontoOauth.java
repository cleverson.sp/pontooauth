package tech.mastertech.pontooauth.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class PontoOauth {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private String funcional;
	@NotNull
	@JsonProperty(access = Access.WRITE_ONLY)
	private String senha;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFuncional() {
		return funcional;
	}

	public void setFuncional(String funcional) {
		this.funcional = funcional;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String nome) {
		this.senha = nome;
	}

}
