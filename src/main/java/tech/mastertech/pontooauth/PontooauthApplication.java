package tech.mastertech.pontooauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PontooauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(PontooauthApplication.class, args);
	}

}
